import "./index.css";

function CustomButton({ action, value, loading }) {
	return (
		<button
			className="app__button"
			type="submit"
			onClick={action}
			value={value}
		>
			{loading ? <div className="app__loader"></div> : value}
		</button>
	);
}

export default CustomButton;