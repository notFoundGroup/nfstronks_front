import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import OrdersIcon from '@mui/icons-material/FormatListBulleted';
import HomeIcon from "@mui/icons-material/Home";
import PersonPinIcon from "@mui/icons-material/PersonPin";
import GroupIcon from "@mui/icons-material/Group";
import useGlobal from "../../hooks/global/useGlobal";
import AddIcon from "@mui/icons-material/Add";
import VerifiedIcon from '@mui/icons-material/Verified';

export default function CustomTabs() {
  const [value, setValue] = useState(window.location.pathname);
  const { isAdmin } = useGlobal();

  const admin = isAdmin();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {

  }, [value]);

  return (
    <Tabs
      scrollButtons
      allowScrollButtonsMobile
      sx={{
        backgroundColor: "var(--dark-gray-1)",
        position: "fixed",
        bottom: 0,
        left: 0,
        right: 0,
        '& .MuiTab-root': {
          overflow: "scroll !important",
          color: "var(--medium-gray) !important",
          justifyContent: 'space-around',

        },
        '& .Mui-selected': {
          color: 'var(--primary) !important',
        },
        '& .MuiTabs-flexContainer': {
          justifyContent: 'space-between'
        }
      }}
      value={value}
      indicatorColor="none"
      variant={admin ? "scrollable" : "fullWidth"}
      onChange={handleChange}>
      <Tab icon={<HomeIcon />} label="Browse" value="/" component={Link} to="/" />
      <Tab icon={<VerifiedIcon />} label="VERIFY JWT" value="/verify-token" component={Link} to="/verify-token" />
      {admin && <Tab icon={<OrdersIcon />} label="Orders" value="/orders" component={Link} to="/orders" />}
      {admin && <Tab icon={<GroupIcon />} label="Users" value="/users" component={Link} to="/users" />}
      {admin && <Tab icon={<AddIcon />} label="Product" value="/products/register" component={Link} to="/products/register" />}
      <Tab icon={<PersonPinIcon />} label="Profile" value="/profile" component={Link} to="/profile" />
    </Tabs>
  );
}