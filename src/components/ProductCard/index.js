import "../../css/layout.css";
import "./index.css";

function MediaCard({ image, title, id, action }) {
    return (
        <div className="flex-row align-end card__image"
            key={id}
            onClick={action}
            style={{
                background: `linear-gradient(to top, rgba(68, 95, 190, 1), rgba(193, 107, 169, 0.3)), url(${image})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "cover"
            }}>
            <p className="card__title">
                {title}
            </p>
        </div>
    );
}

export default MediaCard;