import "./index.css";

function CustomField({ title, value }) {
    return (
        <div className="flex-row justify-between field">
            <span className="field__title">{title}</span>
            <span className="field__value">{value}</span>
        </div>
    );
}

export default CustomField;