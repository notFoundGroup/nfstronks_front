import "../../css/layout.css";
import "./index.css";

function AppLoadScreenSpinner() {
    return (
        <div className="flex-row justify-center self-center load__screen">
            <div className="load__screen__spinner"></div>
        </div>
    );
}


export default AppLoadScreenSpinner;