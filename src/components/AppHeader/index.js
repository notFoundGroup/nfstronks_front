import logo from "../../assets/pixil-frame-0.png";
import "./index.css";

function AppHeader(props) {
    return (
        <div className="app__header">
            <header className="flex-row align-center justify-center">
                <a className="app__header-link" href="/"><img className="logo" src={logo}></img></a>
                <h1 className="title" style={{ fontSize: "24px", fontFamily: "Montserrat, sans-serif" }}>
                    {props.pageTitle}
                </h1>
            </header>
        </div>
    );
}

export default AppHeader;