import { useState } from "react";
import CustomInput from "../CustomInput";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import "./index.css";

export default function CustomPasswordInput({ action, value, inputError }) {
	const [showPassword, setShowPassword] = useState(false);

	function handleClickPassword() {
		setShowPassword(!showPassword);
	}

	return (
		<div style={{ position: 'relative' }}>
			<CustomInput
				id="custom-input-password"
				placeholder="******"
				action={action}
				type={showPassword ? "text" : "password"}
				value={value}
				inputError={inputError}
			/>
			<span className="app__password-icon" onClick={handleClickPassword}>
				{showPassword ? (
					<VisibilityIcon style={{ fill: "#747488" }} />
				) : (
					<VisibilityOffIcon style={{ fill: "#747488" }} />
				)}
			</span>
		</div>
	);
}