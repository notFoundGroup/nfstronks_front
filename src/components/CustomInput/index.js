import TextField from '@mui/material/TextField';

function CustomInputText({
    placeholder,
    value,
    action,
    type,
    inputError,
    readOnlyValue,
    endAdornment
}) {
    return (
        <TextField
            sx={{
                width: '100%',
                '& .MuiInputBase-root': {
                    color: 'var(--primary)',
                    borderRadius: '16px !important',
                    '& .MuiOutlinedInput-notchedOutline': {
                        borderColor: 'var(--dark-gray-1)'
                    },
                    '& .MuiOutlinedInput-notchedOutline:hover': {

                        borderColor: 'var(--dark-gray-1)'
                    }
                },
                '& .MuiFormHelperText-root': {
                    color: 'var(--gray)'
                },
            }}
            error={inputError ? true : false}
            variant="outlined"
            placeholder={placeholder}
            size="small"
            value={value}
            onChange={action}
            type={type}
            helperText={inputError && inputError}
            InputProps={{
                readOnly: readOnlyValue,
                endAdornment: endAdornment
            }}
        />
    );
}

export default CustomInputText;