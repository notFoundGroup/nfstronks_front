import { useRef } from "react";
import useProducts from "../../hooks/products/useProducts";
import "./index.css";

function CustomInputTypeFile() {
	const { setProductImage } = useProducts();
	const hiddenFileInput = useRef(null);

	const handleClick = event => {
		hiddenFileInput.current.click();
	};

	function handleChange(e) {
		const fileUploaded = e.target.files[0];
		setProductImage(fileUploaded);
	}

	return (
		<label className="input-file__label" htmlFor="img">
			<input id="img"
				type="file"
				name="img"
				accept="image/*"
				ref={hiddenFileInput}
				onChange={handleChange}
				required
			/>
		</label>
	)
}

export default CustomInputTypeFile;