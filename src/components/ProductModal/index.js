import AppButton from "../AppButton";
import useGlobal from "../../hooks/global/useGlobal";
import useOrdersRequests from "../../hooks/orders/useOrdersRequests";
import "../../css/layout.css";
import "./index.css";
import { useNavigate } from "react-router-dom";

function ProductModal({ id, title, author, price, action }) {
    const { token } = useGlobal();
    const ordersRequests = useOrdersRequests();
    const navigate = useNavigate();

    function handleSubmitOrder(id) {
        if (!token) {
            navigate("/login");
        } else {
            ordersRequests.register(id);
        }
    }

    return (
        <div className="product__modal-background flex-row align-center justify-center">
            <div className="product__modal flex-column align-center justify-center" style={{ gap: "15px" }}>
                <span>
                    Buy "{title}" by {author}?
                </span>

                <h2 className="product-info__price">
                    ${price}
                </h2>
                <div className="flex-row justify-between align-center" style={{ width: "100%" }}>
                    <a href="#" onClick={action}>No</a>
                    <AppButton
                        value="CONFIRM"
                        action={() => handleSubmitOrder(id)}
                    />
                </div>
            </div>
        </div>
    );
}

export default ProductModal;