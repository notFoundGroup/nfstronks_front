import formatProductTitleToURL from "../../helpers/formatProductTitleToURL";
import "./index.css";

function ProductImage({ title }) {
    return (
        <img className="product__image"
            src={`https://nfstronks-products.s3.amazonaws.com/${formatProductTitleToURL(title != null ? title : "")}`}
            alt={title}
        />
    );
}

export default ProductImage;