import './index.css';

function FieldsList({ name }) {
    return (
        <div className="orders__container">
            <p className="order__field">{name}</p>
        </div>
    );
}

export default FieldsList;