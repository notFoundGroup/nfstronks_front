import { Navigate, Outlet } from "react-router-dom";
import useGlobal from "../../hooks/global/useGlobal";

function ProtectedRoutes() {
    const { token } = useGlobal();
    return token ? <Outlet /> : <Navigate to="/login" />;
}

export default ProtectedRoutes;