import AppHeader from "../../components/AppHeader";
import "./index.css";
import "../../css/layout.css";

function Search() {
	return (
		<div className="search__container">
			<AppHeader pageTitle="Search" />
		</div >
	)
}

export default Search;