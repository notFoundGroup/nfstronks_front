import FieldsList from "../../components/FieldsList";
import useUsersRequests from "../../hooks/users/useUsersRequests";
import useUsers from "../../hooks/users/useUsers";
import AppHeader from "../../components/AppHeader";
import { useEffect } from "react";
import "./index.css";
import React from "react";

function Users() {
	const usersRequests = useUsersRequests();

	const { users } = useUsers();

	useEffect(() => {
		usersRequests.getUsers();
	}, []);

	return (
		<div className="user__container">
			<AppHeader pageTitle="Users" />
			<div className="users_list">
				{users.map((user) => (
					<FieldsList name={user.name} />
				))}
			</div>
		</div >
	)
}

export default Users;