import AppHeader from "../../components/AppHeader";
import AppButton from "../../components/AppButton";
import CustomInputText from "../../components/CustomInput";
import CustomInputPassword from "../../components/InputPassword";
import useUsersHandlers from "../../hooks/users/useUsersHandlers";
import useUsers from "../../hooks/users/useUsers";
import "../../css/layout.css";
import "./index.css";

function SignUpPage() {
	const {
		name,
		setName,
		nameError,
		birthDate,
		setBirthDate,
		birthDateError,
		cpf,
		setCpf,
		cpfError,
		email,
		setEmail,
		emailError,
		phone,
		setPhone,
		phoneError,
		password,
		setPassword,
		confirmedPassword,
		setConfirmedPassword,
		confirmedPasswordError,
		passwordError,
		loading
	} = useUsers();

	const { handleSignUpSubmit } = useUsersHandlers();

	return (
		<div className="signup__page">
			<AppHeader pageTitle="Sign Up" />
			<form className="flex-column signup__form">
				<div className="flex-column" style={{ gap: '12px' }}>
					<div className="flex-column signup__inputs">
						<label htmlFor="input-name">Name</label>
						<CustomInputText
							value={name}
							action={(e) => setName(e.target.value)}
							placeholder="Name Lastname"
							inputError={nameError}
						/>
					</div>

					<div className="flex-column signup__inputs">
						<label htmlFor="input-cpf">CPF</label>
						<CustomInputText
							value={cpf}
							action={(e) => setCpf(e.target.value)}
							placeholder="00000000000"
							inputError={cpfError}
						/>
					</div>

					<div className="flex-column signup__inputs">
						<label htmlFor="input-birthdate">Birthdate</label>
						<CustomInputText
							value={birthDate}
							action={(e) => setBirthDate(e.target.value)}
							placeholder="00/00/0000"
							type="date"
							inputError={birthDateError}
						/>
					</div>

					<div className="flex-column signup__inputs">
						<label htmlFor="input-phone">Phone number</label>
						<CustomInputText
							value={phone}
							action={(e) => setPhone(e.target.value)}
							placeholder="11000000000"
							inputError={phoneError}
						/>
					</div>

					<div className="flex-column signup__inputs">
						<label htmlFor="input-email">E-mail</label>
						<CustomInputText
							value={email}
							action={(e) => setEmail(e.target.value)}
							placeholder="youremail@email.com"
							inputError={emailError}
						/>
					</div>

					<div className="flex-column signup__inputs">
						<label htmlFor="input-password">Password</label>
						<CustomInputPassword
							value={password}
							action={(e) => setPassword(e.target.value)}
							inputError={passwordError}
						/>
					</div>

					<div className="flex-column signup__inputs">
						<label htmlFor="input-confirmed_password">Confirm password</label>
						<CustomInputPassword
							value={confirmedPassword}
							action={(e) => setConfirmedPassword(e.target.value)}
							inputError={confirmedPasswordError}
						/>
					</div>
				</div>
				<span>For an admin account, contact our support.</span>
				<AppButton
					value="SIGNUP"
					action={(e) => handleSignUpSubmit(e)}
					loading={loading}
				/>
			</form>
		</div>
	)
}

export default SignUpPage;