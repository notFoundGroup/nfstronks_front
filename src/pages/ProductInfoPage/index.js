import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import AppHeader from "../../components/AppHeader";
import AppButton from "../../components/AppButton";
import CustomField from "../../components/CustomField";
import ProductImage from "../../components/ProductImage";
import ProductModal from "../../components/ProductModal";
import useGlobal from "../../hooks/global/useGlobal";
import useProducts from "../../hooks/products/useProducts";
import useProductsRequests from "../../hooks/products/useProductsRequests";
import formatDateFromDatabase from "../../helpers/formatDateFromDatabase";
import "../../css/layout.css";
import "./index.css";
import AppLoadScreenSpinner from "../../components/AppLoadScreenSpinner";

function ProductInfo() {
    const { toggleModal, setToggleModal } = useGlobal();
    const { loading, productInfo } = useProducts();
    const productsRequests = useProductsRequests();
    const { id } = useParams();

    useEffect(() => {
        productsRequests.getById(id);
    }, []);

    return (
        <div className="product-detail__page">
            <AppHeader pageTitle="Artwork" />

            {loading && <AppLoadScreenSpinner />}

            {toggleModal &&
                <ProductModal
                    id={id}
                    title={productInfo.title}
                    author={productInfo.author}
                    price={productInfo.price}
                    action={() => setToggleModal(false)}
                />
            }

            {!loading &&
                <div className="flex-column align-center product-info__container">
                    <ProductImage
                        title={productInfo.title}
                    />

                    <div className="flex-column justify-center" style={{ width: "90%", maxWidth: "900px" }}>
                        <h1 className="product-info__title">{productInfo.title}</h1>
                        <span className="product-info__author">By {productInfo.author}</span>

                        <p className="product-info__description">{productInfo.description}</p>

                        <div className="flex-column" style={{ gap: "12px" }}>
                            <CustomField title="Country" value={productInfo.country} />
                            <CustomField title="Launch date" value={formatDateFromDatabase(productInfo.launchDate)} />
                        </div>

                        <div className="flex-row justify-between product-info__action">
                            <h2 className="product-info__price">${productInfo.price}</h2>
                            <AppButton
                                value={"BUY NOW"}
                                action={() => setToggleModal(true)}
                            />
                        </div>
                    </div>
                </div>
            }
        </div>
    );
}

export default ProductInfo;