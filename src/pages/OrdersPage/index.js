import AppHeader from "../../components/AppHeader";
import FieldsList from "../../components/FieldsList";
import useOrdersRequests from "../../hooks/orders/useOrdersRequests";
import useOrders from "../../hooks/orders/useOrders";
import { useEffect } from "react";
import "./index.css";

function Orders() {
	const ordersRequests = useOrdersRequests();

	const { orders } = useOrders();

	useEffect(() => {
		ordersRequests.getAll();
	}, []);

	return (
		<div className="order__container">
			<AppHeader pageTitle="Orders" />
			<div className="orders_list">
				{orders && orders.map((order) => (
					<FieldsList name={`Order #${order.id}`} />
				))}
			</div>
		</div>
	)
}

export default Orders;