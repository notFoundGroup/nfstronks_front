import { useEffect } from "react";
import { Link } from "react-router-dom";
import AppHeader from "../../components/AppHeader";
import ProductCard from "../../components/ProductCard";
import AppLoadScreenSpinner from "../../components/AppLoadScreenSpinner";
import useProducts from "../../hooks/products/useProducts";
import useGlobal from "../../hooks/global/useGlobal";
import useProductsRequests from "../../hooks/products/useProductsRequests";
import formatProductTitleToURL from "../../helpers/formatProductTitleToURL";
import "../../css/layout.css";
import "./index.css";

function Products() {
    const { products, loading } = useProducts();
    const { isAdmin } = useGlobal();
    const productsRequests = useProductsRequests();

    useEffect(() => {
        if (isAdmin()) {
            productsRequests.getAll();
        } else {
            productsRequests.getAllAvailable();
        }
    }, []);

    return (
        <div className="products__page">
            <AppHeader pageTitle="Products" />
            <div className="flex-column justify-center align-center products__list">
                {loading && <AppLoadScreenSpinner />}

                {!loading && products.map((product) => (
                    <Link className="products__link" to={`/products/${product.id}`}>
                        <ProductCard
                            key={product.title}
                            image={`https://nfstronks-products.s3.amazonaws.com/${formatProductTitleToURL(product.title)}`}
                            title={product.title}
                        />
                    </Link>
                ))}
            </div>
        </div>
    );
}

export default Products;