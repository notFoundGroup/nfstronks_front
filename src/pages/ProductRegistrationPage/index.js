import AppButton from "../../components/AppButton";
import AppHeader from "../../components/AppHeader";
import CustomInputText from "../../components/CustomInput";
import CustomInputTypeFile from "../../components/CustomInputTypeFile";
import useProducts from "../../hooks/products/useProducts";
import useProductsHandlers from "../../hooks/products/useProductsHandlers";
import "../../css/layout.css";
import "./index.css";

function ProductRegistrationPage() {
    const {
        loading,
        productTitle,
        setProductTitle,
        productTitleError,
        productAuthor,
        setProductAuthor,
        productAuthorError,
        productDescription,
        setProductDescription,
        productDescriptionError,
        productCountry,
        setProductCountry,
        productCountryError,
        productLaunchDate,
        setProductLaunchDate,
        productLaunchDateError,
        productPrice,
        setProductPrice,
        productPriceError,
        productImage
    } = useProducts();
    const { handleProductSubmit } = useProductsHandlers();

    return (
        <div className="product-registration__page">
            <AppHeader pageTitle="+ Product" />

            <form className="flex-column product-registration__form">
                <div className="flex-column" style={{ gap: '12px', marginBottom: '20px' }}>
                    <div className="flex-column product-registration__inputs">
                        <CustomInputTypeFile />
                    </div>

                    <div className="flex-column product-registration__inputs">
                        <label htmlFor="input-title">Title</label>
                        <CustomInputText
                            value={productTitle}
                            action={(e) => setProductTitle(e.target.value)}
                            placeholder="Cool Title"
                            inputError={productTitleError}
                        />
                    </div>

                    <div className="flex-column product-registration__inputs">
                        <label htmlFor="input-author">Author</label>
                        <CustomInputText
                            value={productAuthor}
                            action={(e) => setProductAuthor(e.target.value)}
                            placeholder="Artist Name"
                            inputError={productAuthorError}
                        />
                    </div>

                    <div className="flex-column product-registration__inputs">
                        <label htmlFor="input-description">Description</label>
                        <CustomInputText
                            value={productDescription}
                            action={(e) => setProductDescription(e.target.value)}
                            placeholder="Cool description about this image"
                            inputError={productDescriptionError}
                        />
                    </div>

                    <div className="flex-column product-registration__inputs">
                        <label htmlFor="input-country">Country</label>
                        <CustomInputText
                            value={productCountry}
                            action={(e) => setProductCountry(e.target.value)}
                            placeholder="BR"
                            inputError={productCountryError}
                        />
                    </div>

                    <div className="flex-column product-registration__inputs">
                        <label htmlFor="input-launchdate">Launch date</label>
                        <CustomInputText
                            value={productLaunchDate}
                            action={(e) => setProductLaunchDate(e.target.value)}
                            type="date"
                            inputError={productLaunchDateError}
                        />
                    </div>

                    <div className="flex-column product-registration__inputs">
                        <label htmlFor="input-price">Price</label>
                        <CustomInputText
                            value={productPrice}
                            action={(e) => setProductPrice(e.target.value)}
                            type="number"
                            placeholder="100000"
                            inputError={productPriceError}
                        />
                    </div>
                </div>

                <AppButton
                    value="REGISTER"
                    action={(e) => handleProductSubmit(e)}
                    loading={loading}
                />
            </form>
        </div>
    )
}

export default ProductRegistrationPage;
