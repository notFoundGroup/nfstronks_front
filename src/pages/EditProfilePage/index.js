import { useEffect } from "react";
import AppButton from "../../components/AppButton";
import AppHeader from "../../components/AppHeader";
import CustomInputText from "../../components/CustomInput";
import CustomInputPassword from "../../components/InputPassword";
import useUsers from "../../hooks/users/useUsers";
import useUsersHandlers from "../../hooks/users/useUsersHandlers";
import useUsersRequests from "../../hooks/users/useUsersRequests";
import formatDateFromDatabase from "../../helpers/formatDateFromDatabase";
import "../../css/layout.css";
import "./index.css";

function EditProfilePage() {
    const {
        currentUser,
        name,
        setName,
        nameError,
        birthDate,
        setBirthDate,
        birthDateError,
        cpf,
        setCpf,
        cpfError,
        email,
        setEmail,
        emailError,
        phone,
        setPhone,
        phoneError,
        password,
        setPassword,
        confirmedPassword,
        setConfirmedPassword,
        confirmedPasswordError,
        passwordError,
        loading,
        setLoading
    } = useUsers();

    const usersRequests = useUsersRequests();

    const { handleUpdateProfileSubmit } = useUsersHandlers();

    useEffect(() => {
        setLoading(true);

        usersRequests.getCurrentUser();

        setName(currentUser.name);
        setCpf(currentUser.cpf);
        setBirthDate(formatDateFromDatabase("edit", currentUser.birthDate));
        setEmail(currentUser.email);
        setPhone(currentUser.phone);
    }, [currentUser.name]);

    return (
        <div className="edit-profile__page">
            <AppHeader pageTitle="Edit Profile" />
            {loading && <div className="load__screen">
                <div className="load__screen__spinner"></div>
            </div>}

            {!loading &&
                <form className="flex-column edit-profile__form">
                    <div className="flex-column" style={{ gap: '12px' }}>
                        <div className="flex-column edit-profile__inputs">
                            <label htmlFor="input-name">Name</label>
                            <CustomInputText
                                value={name}
                                action={(e) => setName(e.target.value)}
                                placeholder="Name Lastname"
                                inputError={nameError}
                            />
                        </div>

                        <div className="flex-column edit-profile__inputs">
                            <label htmlFor="input-cpf">CPF</label>
                            <CustomInputText
                                value={cpf}
                                action={(e) => setCpf(e.target.value)}
                                type="number"
                                placeholder="00000000000"
                                inputError={cpfError}
                            />
                        </div>

                        <div className="flex-column edit-profile__inputs">
                            <label htmlFor="input-birthdate">Birthdate</label>
                            <CustomInputText
                                value={birthDate}
                                action={(e) => setBirthDate(e.target.value)}
                                type="date"
                                inputError={birthDateError}
                            />
                        </div>

                        <div className="flex-column edit-profile__inputs">
                            <label htmlFor="input-phone">Phone number</label>
                            <CustomInputText
                                value={phone}
                                action={(e) => setPhone(e.target.value)}
                                placeholder="11000000000"
                                inputError={phoneError}
                            />
                        </div>

                        <div className="flex-column edit-profile__inputs">
                            <label htmlFor="input-email">E-mail</label>
                            <CustomInputText
                                value={email}
                                action={(e) => setEmail(e.target.value)}
                                placeholder="youremail@email.com"
                                inputError={emailError}
                            />
                        </div>

                        <div className="flex-column signup__inputs">
                            <label htmlFor="input-password">Password</label>
                            <CustomInputPassword
                                value={password}
                                action={(e) => setPassword(e.target.value)}
                                inputError={passwordError}
                            />
                        </div>

                        <div className="flex-column signup__inputs">
                            <label htmlFor="input-confirmed_password">Confirm password</label>
                            <CustomInputPassword
                                value={confirmedPassword}
                                action={(e) => setConfirmedPassword(e.target.value)}
                                inputError={confirmedPasswordError}
                            />
                        </div>
                    </div>

                    <div className="flex-row justify-between align-center">
                        <a href="/profile">Go back</a>
                        <AppButton
                            value="SAVE"
                            action={(e) => handleUpdateProfileSubmit(e)}
                            loading={loading}
                        />
                    </div>
                </form>
            }
        </div>
    )
}

export default EditProfilePage;