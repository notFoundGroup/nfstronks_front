import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AppHeader from "../../components/AppHeader";
import AppButton from "../../components/AppButton";
import CustomField from "../../components/CustomField";
import IconAvatar from "@mui/icons-material/AccountCircle";
import admImg from "../../assets/kingMonkey.jpeg";
import useUsers from "../../hooks/users/useUsers";
import useUsersRequests from "../../hooks/users/useUsersRequests";
import useGlobal from "../../hooks/global/useGlobal";
import formatDateFromDatabase from "../../helpers/formatDateFromDatabase";
import "../../css/layout.css";
import "./index.css";

function UserProfile() {
	const { currentUser, loading, setLoading } = useUsers();
	const userRequest = useUsersRequests();
	const { isAdmin, removeToken } = useGlobal();
	const admin = isAdmin();
	const navigate = useNavigate();

	function handleLogout() {
		removeToken();
		navigate("/");
	}

	useEffect(() => {
		setLoading(true);
		userRequest.getCurrentUser();
	}, []);

	return (
		<div className="profile__container">
			<AppHeader pageTitle="Profile" />

			{!admin && <div className="profile__avatar">
				{<IconAvatar sx={{ fontSize: 80 }} />}
			</div>}

			{admin && <div className="profile__avatar">
				{<img className="admImg" src={admImg} alt="admin profile" />}
				<h1>ADM</h1>
			</div>}


			{loading && <div className="load__screen">
				<div className="load__screen__spinner"></div>
			</div>}

			{!loading && currentUser &&
				<div className="flex-column justify-center profile__fields">
					<CustomField
						key="name"
						title="Name"
						value={currentUser.name}
					/>
					<CustomField
						key="birthdate"
						title="Birthdate"
						value={formatDateFromDatabase("profile", currentUser.birthDate)}
					/>
					<CustomField
						key="CPF"
						title="CPF"
						value={currentUser.cpf}
					/>
					<CustomField
						key="email"
						title="Email"
						value={currentUser.email}
					/>
					<CustomField
						key="phone"
						title="Phone"
						value={currentUser.phone}
					/>

					<CustomField
						className="self-center"
						key="link"
						title={<a href="/verify-token">Verify image token</a>}
					/>

					<div className="flex-row align-center justify-between" style={{ marginTop: "20px" }}>
						<div className="flex-row align-center">
							<a href="/" onClick={handleLogout}>Logout</a>
						</div>
						<a className="profile__link" href="/profile/edit"><AppButton value="EDIT" /></a>
					</div>
				</div>
			}
		</div>
	);
}

export default UserProfile;