import AppHeader from "../../components/AppHeader";
import useProducts from "../../hooks/products/useProducts";
import FingerprintIcon from "@mui/icons-material/Fingerprint";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import CustomInputText from "../../components/CustomInput";
import ProductImage from "../../components/ProductImage";
import useProductsRequests from "../../hooks/products/useProductsRequests";
import formatProductTitleToURL from "../../helpers/formatProductTitleToURL";
import "./index.css";
import { useEffect } from "react";

function VerifyTokenPage() {
    const { productToken, setProductToken, productTitle, loading } = useProducts();
    const productsRequests = useProductsRequests();

    function handleTokenSubmit(token) {
        productsRequests.verifyToken(token);
    }

    useEffect(() => {

    }, [productTitle]);

    return (
        <div className="verify-token__container">
            <AppHeader pageTitle="Verify JWT" />

            <div className="verify-token__content">
                <div className="flex-column verify-token__input">
                    <label htmlFor="input-token">Token</label>
                    <CustomInputText
                        key="token"
                        title="Token"
                        value={productToken}
                        action={(e) => setProductToken(e.target.value)}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    onClick={() => handleTokenSubmit(productToken)}
                                    edge="end"
                                >
                                    <FingerprintIcon sx={{ color: "#fff" }} />
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </div>

                {loading && <div className="load__screen">
                    <div className="load__screen__spinner"></div>
                </div>}

                {!loading && productTitle &&
                    <div>
                        <h1 className="successMessage">Congratulations, you own the {productTitle} image!!!
                        </h1>
                        <a href={`https://nfstronks-products.s3.amazonaws.com/${formatProductTitleToURL(productTitle)}`}>
                            <ProductImage
                                title={productTitle}
                            />
                        </a>
                    </div>
                }
            </div>
        </div>
    );
}

export default VerifyTokenPage;