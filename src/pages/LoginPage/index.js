import { Link } from "react-router-dom";
import useLogin from "../../hooks/login/useLogin";
import useLoginHandlers from "../../hooks/login/useLoginHandlers";
import AppButton from "../../components/AppButton";
import CustomInputText from "../../components/CustomInput";
import CustomInputPassword from "../../components/InputPassword";
import AppHeader from "../../components/AppHeader";
import "../../css/layout.css";
import "./index.css";

function Login() {
	const { email, setEmail, emailError, password, setPassword, passwordError, loading } = useLogin();
	const { handleSubmit } = useLoginHandlers();

	return (
		<div className="login__container">
			<AppHeader pageTitle="Login" />
			<form className="flex-column login__form">
				<div className="flex-column" style={{ gap: '12px' }}>
					<div className="flex-column login__inputs">
						<label htmlFor="input-email">E-mail</label>
						<CustomInputText
							value={email}
							action={(e) => setEmail(e.target.value)}
							placeholder="youremail@email.com"
							inputError={emailError}
						/>
					</div>

					<div className="flex-column login__inputs">
						<label htmlFor="input-password">Password</label>
						<CustomInputPassword
							value={password}
							action={(e) => setPassword(e.target.value)}
							inputError={passwordError}
						/>
					</div>
				</div>

				<AppButton
					value="LOGIN"
					action={(e) => handleSubmit(e)}
					loading={loading}
				/>

				<div className="flex-row justify-center align-center">
					<p>
						Don't have an account?
					</p>
					<Link to="/signup" style={{ marginLeft: '8px' }}>
						Sign up
					</Link>
				</div>
			</form >
		</div >
	)
}


export default Login;