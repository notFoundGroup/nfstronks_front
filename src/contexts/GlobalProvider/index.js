import useGlobalProvider from "../../hooks/global/useGlobalProvider";
import GlobalContext from "../GlobalContext";

function GlobalProvider(props) {
    const global = useGlobalProvider();
    return <GlobalContext.Provider value={global}>{props.children}</GlobalContext.Provider>;
}

export default GlobalProvider;