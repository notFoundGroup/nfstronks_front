import useLoginProvider from "../../hooks/login/useLoginProvider";
import LoginContext from "../LoginContext";

function LoginProvider(props) {
    const login = useLoginProvider();
    return <LoginContext.Provider value={login}>{props.children}</LoginContext.Provider>;
}

export default LoginProvider;