import useOrdersProvider from "../../hooks/orders/useOrdersProvider";
import OrdersContext from "../OrdersContext";

function OrdersProvider(props) {
    const orders = useOrdersProvider();
    return <OrdersContext.Provider value={orders}>{props.children}</OrdersContext.Provider>;
}

export default OrdersProvider;