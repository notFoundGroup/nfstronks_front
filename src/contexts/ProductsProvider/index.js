import useProductsProvider from "../../hooks/products/useProductsProvider";
import ProductsContext from "../ProductsContext";

function ProductsProvider(props) {
    const products = useProductsProvider();
    return <ProductsContext.Provider value={products}>{props.children}</ProductsContext.Provider>;
}

export default ProductsProvider;