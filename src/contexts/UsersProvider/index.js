import useUsersProvider from "../../hooks/users/useUsersProvider";
import UsersContext from "../UsersContext";

function UsersProvider(props) {
    const users = useUsersProvider();
    return <UsersContext.Provider value={users}>{props.children}</UsersContext.Provider>;
}

export default UsersProvider;