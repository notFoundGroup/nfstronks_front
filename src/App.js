import { BrowserRouter, Routes, Route } from "react-router-dom";
import AppTabs from "../src/components/AppTabs";
import ProtectedRoutes from "./components/ProtectedRoutes";
import GlobalProvider from "./contexts/GlobalProvider";
import LoginProvider from "./contexts/LoginProvider";
import ProductsProvider from "./contexts/ProductsProvider";
import OrdersProvider from "./contexts/OrdersProvider";
import UsersProvider from "./contexts/UsersProvider";
import UserProfilePage from "./pages/UserProfilePage";
import EditProfilePage from "./pages/EditProfilePage";
import ProductInfoPage from "./pages/ProductInfoPage";
import ProductsPage from "./pages/ProductsPage";
import ProductRegistrationPage from "./pages/ProductRegistrationPage";
import VerifyTokenPage from "./pages/VerifyTokenPage";
import LoginPage from "./pages/LoginPage";
import OrdersPage from "./pages/OrdersPage";
import SearchPage from "./pages/SearchPage";
import SignUpPage from "./pages/SignUpPage";
import UsersPage from "./pages/UsersPage";
import "./index.css";

function App() {

  return (
    <div className="App">
      <GlobalProvider>
        <LoginProvider>
          <OrdersProvider>
            <UsersProvider>
              <ProductsProvider>
                <BrowserRouter>
                  <Routes>
                    <Route path="/login" element={<LoginPage />} />
                    <Route path="/signup" element={<SignUpPage />} />
                    <Route exact path="/" element={<ProductsPage />} />
                    <Route path="/search" element={<SearchPage />} />
                    <Route path="/verify-token" element={<VerifyTokenPage />} />
                    <Route path="/products/:id" element={<ProductInfoPage />} />
                    <Route path="" element={<ProtectedRoutes />}>
                      <Route path="/profile" element={<UserProfilePage />} />
                      <Route path="/profile/edit" element={<EditProfilePage />} />
                      <Route path="/orders" element={<OrdersPage />} />
                      <Route path="/users" element={<UsersPage />} />
                      <Route path="/products/register" element={<ProductRegistrationPage />} />
                    </Route>
                  </Routes>
                  <AppTabs />
                </BrowserRouter>
              </ProductsProvider>
            </UsersProvider>
          </OrdersProvider>
        </LoginProvider>
      </GlobalProvider>
    </div>
  );
}

export default App;
