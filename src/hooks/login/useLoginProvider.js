import { useState } from "react";

function useLoginProvider() {
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [emailError, setEmailError] = useState(false);
	const [passwordError, setPasswordError] = useState(false);
	const [loading, setLoading] = useState(false);

	return {
		email,
		setEmail,
		password,
		setPassword,
		emailError,
		setEmailError,
		passwordError,
		setPasswordError,
		loading,
		setLoading
	};
}

export default useLoginProvider;