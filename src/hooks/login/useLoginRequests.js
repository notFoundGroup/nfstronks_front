import { useNavigate } from "react-router-dom";
import useLogin from "./useLogin";
import useGlobal from "../global/useGlobal";
import toast from "../../helpers/toast";

function useLoginRequests() {
    const BASE_URL = "http://ac1b993cc89234d74ba2f9abec9ebe7a-2062588149.us-east-1.elb.amazonaws.com:80";

    const {
        email,
        setEmail,
        password,
        setPassword,
        setLoading,
        setEmailError,
        setPasswordError
    } = useLogin();

    const { setToken } = useGlobal();
    const navigate = useNavigate();

    async function login() {
        const body = {
            email,
            password
        };

        try {
            const response = await fetch(`${BASE_URL}/auth/login`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(body),
            });

            const data = await response.json();

            if (!data.token) {
                console.log(data);
                throw new Error(data.message);
            }

            setToken(data.token);
            setEmail("");
            setPassword("");
            setLoading(false);

            toast.messageSuccess("Welcome!");
            navigate("/");
        } catch (error) {
            console.log(error.message);
            if ((/\b(email)\b/gi).test(error.message) || error.message.includes("Usuario")) {
                setEmailError(error.message);
            }

            if (error.message.includes("senha")) {
                setPasswordError(error.message);
            }

            setLoading(false);
        }
    }

    return {
        login
    };
}

export default useLoginRequests;