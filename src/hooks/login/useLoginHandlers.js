import useLogin from "./useLogin";
import useLoginRequests from "./useLoginRequests";
import validateEmail from "../../helpers/validateEmail";

function useLoginHandlers() {
	const {
		email,
		setEmail,
		setEmailError,
		setPassword,
		setPasswordError,
		setLoading,
	} = useLogin();
	const requests = useLoginRequests();

	function handleSubmit(e) {
		e.preventDefault();

		setEmailError("");
		setPasswordError("");

		if (!validateEmail(email)) {
			return setEmailError("Insert a valid e-mail address");
		}

		setLoading(true);

		requests.login();
	}

	function handleLoginInputs() {
		setEmail("");
		setPassword("");
		setEmailError("");
		setPasswordError("");
	}

	return { handleSubmit, handleLoginInputs };
}

export default useLoginHandlers;