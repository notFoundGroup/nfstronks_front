import { useContext } from "react";
import LoginContext from "../../contexts/LoginContext";

function useLogin() {
    return useContext(LoginContext);
}

export default useLogin;