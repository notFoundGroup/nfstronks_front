import { useContext } from "react";
import UsersContext from "../../contexts/UsersContext";

function useUsers() {
    return useContext(UsersContext);
}

export default useUsers;