import { useState } from "react";
import { useNavigate } from "react-router-dom";
import toast from "../../helpers/toast";
import formatDateForDatabase from "../../helpers/formatDateForDatabase";
import useGlobal from "../global/useGlobal";
import useUsers from "./useUsers";

function useUsersRequests() {
	const BASE_URL = "http://ac1b993cc89234d74ba2f9abec9ebe7a-2062588149.us-east-1.elb.amazonaws.com:80";

	const {
		setUsers,
		name,
		setName,
		cpf,
		setCpf,
		email,
		setEmail,
		phone,
		setPhone,
		birthDate,
		setBirthDate,
		password,
		setPassword,
		setConfirmedPassword,
		setLoading,
		setCurrentUser
	} = useUsers();

	const { token } = useGlobal();
	const navigate = useNavigate();
	const [role, setRole] = useState("ROLE_USER");

	async function getCurrentUser() {
		try {
			const response = await fetch(`${BASE_URL}/users/current`, {
				method: "GET",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			});

			const data = await response.json();

			if (!data) {
				throw new Error(data);
			}

			setCurrentUser(data);
			setLoading(false);
		} catch (error) {
			toast.messageError(error.message);
			setLoading(false);
		}
	}

	async function getUsers() {
		try {
			setLoading(true);
			const response = await fetch(`${BASE_URL}/users`, {
				method: "GET",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			});

			const data = await response.json();

			if (!data) {
				throw new Error(data);
			}

			setUsers(data);
			setLoading(false);
		} catch (error) {
			setLoading(false);
			toast.messageError(error.message);
		}
	}

	async function register() {
		const body = {
			name,
			birthDate: birthDate,
			cpf,
			email,
			phone,
			role,
			password
		};

		try {
			const response = await fetch(`${BASE_URL}/users`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(body),
			});

			if (response.status != "201") {
				const data = await response.json();
				throw new Error(data.message);
			}

			setName("");
			setBirthDate("");
			setCpf("");
			setPhone("");
			setEmail("");
			setPassword("");
			setConfirmedPassword("");

			setLoading(false);
			toast.messageSuccess("Account created!");
			navigate("/login");
		} catch (error) {
			setLoading(false);
			toast.messageError(error.message);
		}
	}

	async function update() {
		const body = {
			name,
			birthDate: formatDateForDatabase(birthDate),
			cpf,
			email,
			phone
		};

		if (password) {
			body.password = password;
		}

		try {
			const response = await fetch(`${BASE_URL}/users/current`, {
				method: "PATCH",
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify(body),
			});

			if (response.status !== 204) {
				const data = await response.json();
				throw new Error(data);
			}

			setName("");
			setBirthDate("");
			setCpf("");
			setPhone("");
			setEmail("");
			setPassword("");
			setConfirmedPassword("");

			setLoading(false);
			toast.messageSuccess("Account updated!");
			navigate("/profile");
		} catch (error) {
			console.log(error);
			toast.messageError(error.message);
		}
	}

	return {
		getCurrentUser,
		getUsers,
		register,
		update
	};
}

export default useUsersRequests;