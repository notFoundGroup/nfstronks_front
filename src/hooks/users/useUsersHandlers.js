import useUsers from "./useUsers";
import useUsersRequests from "./useUsersRequests";
import validateEmail from "../../helpers/validateEmail";

function useUsersHandlers() {
    const {
        name,
        cpf,
        birthDate,
        phone,
        email,
        password,
        confirmedPassword,
        setNameError,
        setCpfError,
        setBirthDateError,
        setPhoneError,
        setEmailError,
        setPasswordError,
        setConfirmedPasswordError,
        setLoading
    } = useUsers();

    const usersRequests = useUsersRequests();

    function handleClearInputErrors() {
        setNameError("");
        setCpfError("");
        setBirthDateError("");
        setPhoneError("");
        setEmailError("");
        setPasswordError("");
        setConfirmedPasswordError("");
    }

    function handleInputErrors(mode) {
        if (!name) {
            return setNameError("Name cannot be empty");
        }

        if (!cpf) {
            return setCpfError("CPF cannot be empty");
        }

        if (cpf.length > 11 || cpf.length < 11) {
            return setCpfError("CPF must be 11 digits long");
        }

        if (!birthDate) {
            return setBirthDateError("Birthdate cannot be empty");
        }

        if (!phone) {
            return setPhoneError("Phone cannot be empty");
        }

        if (!email) {
            return setEmailError("E-mail cannot be empty");
        }

        if (!validateEmail(email)) {
            return setEmailError("Insert a valid e-mail address");
        }

        if (mode === "signup") {
            if (!password) {
                return setPasswordError("Password cannot be empty");
            }
        }

        if (password) {
            if (password.length < 8) {
                return setPasswordError("Password should be at least 8 digits long");
            }

            if (!confirmedPassword) {
                return setConfirmedPasswordError("Confirm your chosen password");
            }

            if (password !== confirmedPassword) {
                return setConfirmedPasswordError("Passwords must match");
            }
        }

        return true;
    }

    function handleSignUpSubmit(e) {
        e.preventDefault();

        handleClearInputErrors();

        if (handleInputErrors("signup")) {
            setLoading(true);

            usersRequests.register();
        }
    }

    function handleUpdateProfileSubmit(e) {
        e.preventDefault();

        handleClearInputErrors();

        if (handleInputErrors("update")) {
            setLoading(true);

            usersRequests.update();
        }
    }

    return { handleSignUpSubmit, handleUpdateProfileSubmit };
}

export default useUsersHandlers;