import { useState } from "react";

function useUsersProvider() {
	const [name, setName] = useState("");
	const [cpf, setCpf] = useState("");
	const [phone, setPhone] = useState("");
	const [birthDate, setBirthDate] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmedPassword, setConfirmedPassword] = useState("");

	const [users, setUsers] = useState([]);

	const [currentUser, setCurrentUser] = useState({});

	const [nameError, setNameError] = useState(false);
	const [cpfError, setCpfError] = useState(false);
	const [phoneError, setPhoneError] = useState(false);
	const [birthDateError, setBirthDateError] = useState(false);
	const [emailError, setEmailError] = useState(false);
	const [passwordError, setPasswordError] = useState(false);
	const [confirmedPasswordError, setConfirmedPasswordError] = useState(false);
	const [loading, setLoading] = useState(false);

	return {
		name,
		setName,
		nameError,
		setNameError,
		cpf,
		setCpf,
		cpfError,
		setCpfError,
		phone,
		setPhone,
		phoneError,
		setPhoneError,
		birthDate,
		setBirthDate,
		birthDateError,
		setBirthDateError,
		email,
		setEmail,
		password,
		setPassword,
		confirmedPassword,
		setConfirmedPassword,
		confirmedPasswordError,
		setConfirmedPasswordError,
		users,
		setUsers,
		currentUser,
		setCurrentUser,
		emailError,
		setEmailError,
		passwordError,
		setPasswordError,
		loading,
		setLoading
	};
}

export default useUsersProvider;