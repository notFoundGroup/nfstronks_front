import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "react-use";
import { useJwt } from "react-jwt";
import toast from "../../helpers/toast";

function useGlobalProvider() {
    const [token, setToken, removeToken] = useLocalStorage("token", "");
    const [loading, setLoading] = useState(false);
    const [toggleModal, setToggleModal] = useState(false);
    const { decodedToken, isExpired } = useJwt(token);

    function isAdmin() {
        try {
            if (decodedToken.roles[0] != "ROLE_ADMIN") {
                return false;
            }

            if (isExpired) {
                removeToken();
                toast.messageError("Your session expired...");
            }

            return true;
        } catch (error) {
            return false;
        }
    }

    return {
        token,
        setToken,
        removeToken,
        toggleModal,
        setToggleModal,
        loading,
        setLoading,
        isAdmin
    };
}

export default useGlobalProvider;