import useProducts from "./useProducts";
import useProductsRequests from "./useProductsRequests";

function useProductsHandlers() {
    const {
        productTitle,
        productAuthor,
        productDescription,
        productLaunchDate,
        productCountry,
        productPrice,
        setProductTitleError,
        setProductAuthorError,
        setProductDescriptionError,
        setProductLaunchDateError,
        setProductCountryError,
        setProductPriceError,
        setLoading
    } = useProducts();

    const productsRequests = useProductsRequests();

    function handleClearInputErrors() {
        setProductTitleError(false);
        setProductAuthorError(false);
        setProductDescriptionError(false);
        setProductLaunchDateError(false);
        setProductCountryError(false);
        setProductPriceError(false);
    }

    function handleInputErrors() {
        if (!productTitle) {
            return setProductTitleError("Title cannot be empty");
        }

        if (!productAuthor) {
            return setProductAuthorError("Author cannot be empty");
        }

        if (!productDescription) {
            return setProductDescriptionError("Description cannot be empty");
        }

        if (!productLaunchDate) {
            return setProductLaunchDateError("Launch date cannot be empty");
        }

        if (!productCountry) {
            return setProductCountryError("Country cannot be empty");
        }

        if (!productPrice) {
            return setProductPriceError("Price cannot be empty");
        }

        if (productPrice < 1) {
            return setProductPriceError("Insert a valid value");
        }

        return true;
    }

    function handleProductSubmit(e) {
        e.preventDefault();

        handleClearInputErrors();

        if (handleInputErrors()) {
            setLoading(true);

            productsRequests.register();
        }
    }

    return {
        handleProductSubmit
    }
}

export default useProductsHandlers;