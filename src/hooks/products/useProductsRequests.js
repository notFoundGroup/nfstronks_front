import { useNavigate } from "react-router-dom";
import toast from "../../helpers/toast";
import useProducts from "./useProducts";
import useGlobal from "../global/useGlobal";

function useProductsRequests() {
    const BASE_URL = "http://ac1b993cc89234d74ba2f9abec9ebe7a-2062588149.us-east-1.elb.amazonaws.com:80";

    const {
        setProductTitle,
        setProductImage,
        setProductAuthor,
        setProductDescription,
        setProductLaunchDate,
        setProductCountry,
        setProductPrice,
        setLoading,
        productTitle,
        productAuthor,
        productDescription,
        productLaunchDate,
        productPrice,
        productCountry,
        productImage,
        setProductInfo,
        setProducts
    } = useProducts();

    const { token } = useGlobal();
    const navigate = useNavigate();

    async function register() {
        const nft = productImage;

        const productJson = {
            title: productTitle,
            author: productAuthor,
            description: productDescription,
            launchDate: productLaunchDate,
            price: productPrice,
            isAvailable: true,
            country: productCountry
        };

        const formData = new FormData();
        formData.append("nft", nft);
        formData.append("productJson", JSON.stringify(productJson));

        try {
            setLoading(true);
            const response = await fetch(`${BASE_URL}/products`, {
                method: "POST",
                headers: {
                    Authorization: `${token}`
                },
                body: formData,
            });

            if (response.status !== 201) {
                const data = await response.json();
                throw new Error(data);
            }

            setProductTitle("");
            setProductImage("");
            setProductAuthor("");
            setProductDescription("");
            setProductLaunchDate("");
            setProductCountry("");
            setProductPrice("");
            setLoading(false);

            toast.messageSuccess("New product registered!");
            navigate("/");
        } catch (error) {
            setLoading(false);
            toast.messageError(error.message);
        }
    }

    async function getAll() {
        try {
            setLoading(true);
            const response = await fetch(`${BASE_URL}/products`, {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            const data = await response.json();

            if (!data) {
                throw new Error(data);
            }

            setProducts(data.content);
            setLoading(false);
        } catch (error) {
            toast.messageError(error.message);
        }
    }

    async function getAllAvailable() {
        try {
            setLoading(true);
            const response = await fetch(`${BASE_URL}/products/available`, {
                method: "GET"
            });

            const data = await response.json();

            if (!data) {
                throw new Error(data);
            }

            setProducts(data.content);
            setLoading(false);
        } catch (error) {
            toast.messageError(error.message);
        }
    }

    async function getById(id) {
        try {
            setLoading(true);
            const response = await fetch(`${BASE_URL}/products/${id}`, {
                method: "GET"
            });

            const data = await response.json();

            if (!data) {
                throw new Error(data);
            }

            setProductInfo(data);
            setLoading(false);
        } catch (error) {
            toast.messageError(error.message);
        }
    }

    async function verifyToken(token) {
        try {
            setLoading(true);

            const response = await fetch(`${BASE_URL}/auth/verifyproducttoken/${token}`, {
                method: "POST",
            });

            const data = await response.json();

            if (response.status != 200) {
                throw new Error(data.message);
            }

            setProductTitle(data.title);
            setLoading(false);
        } catch (error) {
            toast.messageError(error.message);
        }
    }

    return {
        register,
        getAll,
        getAllAvailable,
        getById,
        verifyToken
    };
}

export default useProductsRequests;