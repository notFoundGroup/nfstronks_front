import { useState } from "react";

function useProductsProvider() {
    const [productInfo, setProductInfo] = useState({});

    const [productToken, setProductToken] = useState("");
    const [productTitle, setProductTitle] = useState("");
    const [productImage, setProductImage] = useState("");
    const [productAuthor, setProductAuthor] = useState("");
    const [productDescription, setProductDescription] = useState("");
    const [productLaunchDate, setProductLaunchDate] = useState("");
    const [productCountry, setProductCountry] = useState("");
    const [productPrice, setProductPrice] = useState(0);
    const [userProducts, setUserProducts] = useState([]);
    const [products, setProducts] = useState([]);

    const [loading, setLoading] = useState(false);

    const [productTitleError, setProductTitleError] = useState(false);
    const [productAuthorError, setProductAuthorError] = useState(false);
    const [productDescriptionError, setProductDescriptionError] = useState(false);
    const [productLaunchDateError, setProductLaunchDateError] = useState(false);
    const [productCountryError, setProductCountryError] = useState(false);
    const [productPriceError, setProductPriceError] = useState(false);

    return {
        productToken,
        setProductToken,
        productImage,
        setProductImage,
        productInfo,
        setProductInfo,
        productTitle,
        setProductTitle,
        productTitleError,
        setProductTitleError,
        productAuthor,
        setProductAuthor,
        productAuthorError,
        setProductAuthorError,
        productLaunchDate,
        setProductLaunchDate,
        productLaunchDateError,
        setProductLaunchDateError,
        productDescription,
        setProductDescription,
        productDescriptionError,
        setProductDescriptionError,
        productCountry,
        setProductCountry,
        productCountryError,
        setProductCountryError,
        productPrice,
        setProductPrice,
        productPriceError,
        setProductPriceError,
        userProducts,
        setUserProducts,
        products,
        setProducts,
        loading,
        setLoading
    };
}

export default useProductsProvider;