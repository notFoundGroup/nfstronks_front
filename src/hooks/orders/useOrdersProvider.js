import { useState } from "react";

function useOrdersProvider() {
    const [orderInfo, setOrderInfo] = useState({});
    const [userOrders, setUserOrders] = useState([]);
    const [orders, setOrders] = useState([]);

    const [loading, setLoading] = useState(false);

    return {
        orderInfo,
        setOrderInfo,
        userOrders,
        setUserOrders,
        orders,
        setOrders,
        loading,
        setLoading
    };
}

export default useOrdersProvider;