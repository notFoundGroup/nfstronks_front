import { useNavigate } from "react-router-dom";
import toast from "../../helpers/toast";
import useOrders from "./useOrders";
import useGlobal from "../global/useGlobal";

function useOrdersRequests() {
    const BASE_URL = "http://ac1b993cc89234d74ba2f9abec9ebe7a-2062588149.us-east-1.elb.amazonaws.com:80";

    const {
        setOrderInfo,
        setOrders,
        setUserOrders,
        setLoading
    } = useOrders();

    const { token, setToggleModal } = useGlobal();
    const navigate = useNavigate();

    async function register(id) {
        const body = {
            "productId": id
        };

        try {
            const response = await fetch(`${BASE_URL}/orders`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(body),
            });

            if (response.status !== 200) {
                const data = await response.json();
                throw new Error(data.message);
            }

            setLoading(false);
            setToggleModal(false);

            toast.messageSuccess("Order placed! The token was sent to your e-mail.");
            navigate("/products/verify-token");
        } catch (error) {
            setLoading(false);
            toast.messageError(error.message);
        }
    }

    async function getAll() {

        try {
            const response = await fetch(`${BASE_URL}/orders`, {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            const data = await response.json();

            if (!data) {
                throw new Error(data);
            }

            setOrders(data.content);
        } catch (error) {
            toast.messageError(error.message);
        }
    }

    async function getById(id) {
        try {
            const response = await fetch(`${BASE_URL}/orders/${id}`, {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            const data = await response.json();

            if (!data) {
                throw new Error(data);
            }

            setOrderInfo(data);
        } catch (error) {
            toast.messageError(error.message);
        }
    }

    async function getAllByUserId(userId) {
        try {
            const response = await fetch(`${BASE_URL}/orders/user/${userId}`, {
                method: "GET",
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            const data = await response.json();

            if (!data) {
                throw new Error(data);
            }

            setUserOrders(data);
        } catch (error) {
            toast.messageError(error.message);
        }
    }

    return {
        register,
        getAll,
        getById,
        getAllByUserId
    };
}

export default useOrdersRequests;