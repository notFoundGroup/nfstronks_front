import { useContext } from "react";
import OrdersContext from "../../contexts/OrdersContext";

function useOrders() {
    return useContext(OrdersContext);
}

export default useOrders;