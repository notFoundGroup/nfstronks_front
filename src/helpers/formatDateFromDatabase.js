function formatDateFromDatabase(page, timestamp) {
    const dateFromTimestamp = new Date(timestamp);

    const date = dateFromTimestamp.getUTCDate() < 10 ? `0` + dateFromTimestamp.getUTCDate() : dateFromTimestamp.getUTCDate();
    const month = (dateFromTimestamp.getMonth() + 1) < 10 ? `0` + (dateFromTimestamp.getMonth() + 1) : (dateFromTimestamp.getMonth() + 1);
    const year = dateFromTimestamp.getFullYear();

    if (page === "edit") {
        return `${year}-${month}-${date}`;
    } else {
        return `${date}/${month}/${year}`;
    }
}

export default formatDateFromDatabase;