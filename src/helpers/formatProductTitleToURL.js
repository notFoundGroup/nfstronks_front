function formatProductTitleToURL(title) {
    return title.replace(/\s/g, "+");
}

export default formatProductTitleToURL;