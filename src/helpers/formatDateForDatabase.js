function formatDateForDatabase(date) {
    const dateArr = date.split('/');;
    const year = dateArr[2];
    const month = dateArr[1];
    const day = dateArr[0];

    return `${year}-${month}-${day}`;
}

export default formatDateForDatabase;