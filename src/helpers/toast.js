import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();

function messageError(message) {
    toast.error(message, {
        position: "bottom-right",
        autoClose: 6000,
        theme: "colored",
        closeOnClick: true,
        pauseOnHover: false,
        hideProgressBar: true,
        style: {
            whiteSpace: "nowrap",
            color: "#AE1100",
            backgroundColor: "#F2D6D0",
            boxShadow: "0px 4px 42px rgba(0, 0, 0, 0.25)"
        },
    });
}

function messageInfo(message) {
    toast.info(message, {
        position: "bottom-right",
        autoClose: 6000,
        theme: "light",
        closeOnClick: true,
        pauseOnHover: false,
    });
}

function messageSuccess(message) {
    toast.success(message, {
        position: "bottom-right",
        autoClose: 6000,
        theme: "colored",
        closeOnClick: true,
        pauseOnHover: false,
        hideProgressBar: true,
        style: {
            color: "#243F80",
            backgroundColor: "rgba(195, 212, 254, 1)",
        },
    });
}

export default { messageError, messageInfo, messageSuccess };